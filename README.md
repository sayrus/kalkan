# Сервис для подписания SOAP



## Преднастройки

1. В папку keys положить файл gost.p12
2. Необходимо создать файл config.json скопировав с примера config.json.sample


## Поднять через докер

1. Выполнить команду `docker-compose up -d`


## Или поднять без докера

1. Установить openjdk (1.8.0_232)
2. Запусть сервис (kalkan-soap.jar) в терминале командой: java -jar kalkan-soap.jar


## Запрос

1. Кинуть REST запрос, вернется подписанный soap
```json
{
	"action": "http://localhost:3020/sign/",
	"method": "POST",
    "body": "xml",
      "headers": {
        "Content-Type": "text/xml; charset=utf-8"
      }
```
```xml
<?xml version="1.0" encoding="UTF-8"?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
  <S:Body>
    <ns0:sendMessage xmlns:ns0="http://bip.bee.kz/AsyncChannel/v10/Types">
      <request>
        <messageInfo>
          <messageId>5024113e-b70b-479d-8ac7-8c65103e3338</messageId>
          <serviceId>TestService</serviceId>
          <messageType>REQUEST</messageType>
          <messageDate>2018-01-04T19:38:18.518</messageDate>
          <sender>
            <senderId>test</senderId>
            <password>test</password>
          </sender>
        </messageInfo>
        <messageData>
          <data xmlns:ns1="http://schemas.simple.kz/test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns1:TestObject">
            <status>STARTED</status>
          </data>
        </messageData>
      </request>
    </ns0:sendMessage>
  </S:Body>
</S:Envelope>
```

## Создание билда

- Для создания билда в программе Eclipse IDE (4.13.0)
1. File - Export - Java - Runnable Jar File - Next
2. Launch configuration: выбрать MainClass kalkan-soap
3. Export destination: выбрать место и дать имя файлу kalkan-soap.jar
4. Нажать Finish, сформируется kalkan-soap.jar
