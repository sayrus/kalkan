package kz.gov.pki;

import kz.gov.pki.kalkan.asn1.pkcs.PKCSObjectIdentifiers;
import kz.gov.pki.kalkan.jce.provider.KalkanProvider;
import kz.gov.pki.kalkan.xmldsig.KncaXS;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.token.SecurityTokenReference;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.encryption.XMLCipherParameters;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.XMLUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.security.AccessController;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PrivilegedExceptionAction;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.UUID;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class MainClass {
    public static void main(String[] args) throws Exception {
    	String configPath = new File("config.json").getAbsolutePath();
    	JSONParser parser = new JSONParser();
    	String KEYSTORE_KEY, KEYSTORE_PASSWORD;
    	try (Reader reader = new FileReader(configPath)) {
    		JSONObject jsonObject = (JSONObject) parser.parse(reader);
    		KEYSTORE_KEY = (String) jsonObject.get("keystoreKey");
    		KEYSTORE_PASSWORD = (String) jsonObject.get("keystorePassword");
    	}
    	
        HttpServer server = HttpServer.create(new InetSocketAddress(3020), 0);
        server.createContext("/sign", new SignHandler(KEYSTORE_KEY, KEYSTORE_PASSWORD));
        server.createContext("/verify", new VerifyHandler(KEYSTORE_KEY, KEYSTORE_PASSWORD));
        server.setExecutor(null);
        server.start();
    }

    static class SignHandler implements HttpHandler {
    	private String KEYSTORE_KEY;
    	private String KEYSTORE_PASSWORD;
    	
    	SignHandler(String KEYSTORE_KEY, String KEYSTORE_PASSWORD) {
    		this.KEYSTORE_KEY = KEYSTORE_KEY;
    		this.KEYSTORE_PASSWORD = KEYSTORE_PASSWORD;
    	}
    	
        public void handle(HttpExchange httpExchange) throws IOException {
        	System.out.print("asd");
        	
        	InputStreamReader isr =  new InputStreamReader(httpExchange.getRequestBody(), "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            int b;
            StringBuilder buf = new StringBuilder(512);
            while ((b = br.read()) != -1) {
                buf.append((char) b);
            }
            br.close();
            isr.close();
        	String soapToSign = buf.toString();
            
	        KalkanProvider kalkanProvider = new KalkanProvider();
	        Security.addProvider(kalkanProvider);
	        KncaXS.loadXMLSecurity();
  
          try {
              final String signMethod;
              final String digestMethod;
              
              InputStream is = new ByteArrayInputStream(soapToSign.getBytes());

              SOAPMessage msg = MessageFactory.newInstance().createMessage(null, is);
  
              SOAPEnvelope env = msg.getSOAPPart().getEnvelope();
              SOAPBody body = env.getBody();
  
              String bodyId = "id-" + UUID.randomUUID().toString();
              body.addAttribute(new QName(WSConstants.WSU_NS, "Id", WSConstants.WSU_PREFIX), bodyId);
  
              SOAPHeader header = env.getHeader();
              if (header == null) {
                  header = env.addHeader();
              }
              KeyStore store = KeyStore.getInstance("PKCS12", KalkanProvider.PROVIDER_NAME);
              InputStream inputStream;
              inputStream = AccessController.doPrivileged(new PrivilegedExceptionAction<InputStream>() {
                  @Override
                  public FileInputStream run() throws Exception {
                      return new FileInputStream(KEYSTORE_KEY);
                  }
              });
              store.load(inputStream, KEYSTORE_PASSWORD.toCharArray());
              Enumeration<String> als = store.aliases();
              String alias = null;
              while (als.hasMoreElements()) {
                  alias = als.nextElement();
              }
              final PrivateKey privateKey = (PrivateKey) store.getKey(alias, KEYSTORE_PASSWORD.toCharArray());
              final X509Certificate x509Certificate = (X509Certificate) store.getCertificate(alias);
              String sigAlgOid = x509Certificate.getSigAlgOID();
              if (sigAlgOid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption.getId())) {
                  signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha1";
                  digestMethod = Constants.MoreAlgorithmsSpecNS + "sha1";
              } else if (sigAlgOid.equals(PKCSObjectIdentifiers.sha256WithRSAEncryption.getId())) {
                  signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha256";
                  digestMethod = XMLCipherParameters.SHA256;
              } else {
                  signMethod = Constants.MoreAlgorithmsSpecNS + "gost34310-gost34311";
                  digestMethod = Constants.MoreAlgorithmsSpecNS + "gost34311";
              }
  
              Document doc = env.getOwnerDocument();
              Transforms transforms = new Transforms(env.getOwnerDocument());
              transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
  
              Element c14nMethod = XMLUtils.createElementInSignatureSpace(doc, "CanonicalizationMethod");
              c14nMethod.setAttributeNS(null, "Algorithm", Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
  
              Element signatureMethod = XMLUtils.createElementInSignatureSpace(doc, "SignatureMethod");
              signatureMethod.setAttributeNS(null, "Algorithm", signMethod);
  
              XMLSignature sig = new XMLSignature(env.getOwnerDocument(), "", signatureMethod, c14nMethod );
  
              sig.addDocument("#" + bodyId, transforms, digestMethod);
              sig.getSignedInfo().getSignatureMethodElement().setNodeValue(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
  
              WSSecHeader secHeader = new WSSecHeader();
              secHeader.setMustUnderstand(true);
              secHeader.insertSecurityHeader(env.getOwnerDocument());
              secHeader.getSecurityHeader().appendChild(sig.getElement());
              header.appendChild(secHeader.getSecurityHeader());
  
              SecurityTokenReference reference = new SecurityTokenReference(doc);
              reference.setKeyIdentifier(x509Certificate);
  
              sig.getKeyInfo().addUnknownElement(reference.getElement());
              sig.sign(privateKey);
  
              String signedSoap = org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(doc);
  
              verifyXml(signedSoap, x509Certificate);

              byte[] bs = signedSoap.getBytes("UTF-8");
              httpExchange.sendResponseHeaders(200, bs.length);
              OutputStream os = httpExchange.getResponseBody();
              os.write(bs);
              os.close();
            } catch (Exception e) {
            	e.printStackTrace();
            	throw new RuntimeException(e);
            }
        }
    }
    
    static class VerifyHandler implements HttpHandler {
    	private String KEYSTORE_KEY;
    	private String KEYSTORE_PASSWORD;
    	
    	VerifyHandler(String KEYSTORE_KEY, String KEYSTORE_PASSWORD) {
    		this.KEYSTORE_KEY = KEYSTORE_KEY;
    		this.KEYSTORE_PASSWORD = KEYSTORE_PASSWORD;
    	}
    	
        public void handle(HttpExchange httpExchange) throws IOException {
          InputStreamReader isr =  new InputStreamReader(httpExchange.getRequestBody(), "utf-8");
          BufferedReader br = new BufferedReader(isr);
          int b;
          StringBuilder buf = new StringBuilder(512);
          while ((b = br.read()) != -1) {
              buf.append((char) b);
          }
          br.close();
          isr.close();
          
          String soapToVerify = buf.toString();
          
          KalkanProvider kalkanProvider = new KalkanProvider();
          Security.addProvider(kalkanProvider);
          KncaXS.loadXMLSecurity();
  
          try {
              KeyStore store = KeyStore.getInstance("PKCS12", KalkanProvider.PROVIDER_NAME);
              InputStream inputStream;
              inputStream = AccessController.doPrivileged(new PrivilegedExceptionAction<InputStream>() {
                  @Override
                  public FileInputStream run() throws Exception {
                      return new FileInputStream(KEYSTORE_KEY);
                  }
              });
              store.load(inputStream, KEYSTORE_PASSWORD.toCharArray());
              Enumeration<String> als = store.aliases();
              String alias = null;
              while (als.hasMoreElements()) {
                  alias = als.nextElement();
              }
              final X509Certificate x509Certificate = (X509Certificate) store.getCertificate(alias);

              Boolean isValid = verifyXml(soapToVerify, x509Certificate);
              
              String response = isValid.toString();
              byte[] bs = response.getBytes("UTF-8");
              httpExchange.sendResponseHeaders(200, bs.length);
              OutputStream os = httpExchange.getResponseBody();
              os.write(bs);
              os.close();
            } catch (Exception e) {
            	e.printStackTrace();
            	throw new RuntimeException(e);
            }
        }
    }


    public static boolean verifyXml(String xmlString, X509Certificate x509Certificate) {
        boolean result = false;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            Document doc = documentBuilder.parse(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));

            Element sigElement = null;
            Element rootEl = (Element) doc.getFirstChild();

            NodeList list = rootEl.getElementsByTagName("ds:Signature");
            int length = list.getLength();
            System.out.println(length);
            for (int i = 0; i < length; i++) {
                Node sigNode = list.item(length - 1);
                sigElement = (Element) sigNode;
                if (sigElement == null) {
                    System.err.println("Bad signature: Element 'ds:Reference' is not found in XML document");
                }
                XMLSignature signature = new XMLSignature(sigElement, "");
                if (x509Certificate != null) {
                    result = signature.checkSignatureValue(x509Certificate);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("VERIFICATION RESULT IS: " + result);
        return result;
    }
}
